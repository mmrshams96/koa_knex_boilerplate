import User from "../../models/User";
import UserRepo from "../../lib/UserRepo";
import bcrypt from "bcryptjs";
import crypto from "crypto";
import { templateForgetPassword } from "utils";
import {
  Unauthorized,
  encryptPassword,
  generateJWTToken,
  sendEmail,
  NotFound,
} from "helpers";

const userRepo = new UserRepo();

class UserService {
  async login(data) {
    const user = await User.query()
      .findOne({ email: body.email })
      .withGraphFetched("role")
      .catch(() => {
        throw Unauthorized("Unauthorized, User not found");
      });
    const isValid = await bcrypt.compare(body.password, user.password);
    if (!isValid) {
      throw Unauthorized("Unauthorized, password is invalid");
    }
    const parsedUser = user.toJSON();
    return {
      ...parsedUser,
      token: generateJWTToken({
        id: parsedUser.id,
        role_id: parsedUser.role_id,
      }),
    };
  }
  async forget(ctx) {
    const { body } = ctx.request;
    const token = crypto.randomBytes(10).toString("hex");
    await User.query()
      .findOne({ email: body.email })
      .patch({ password_reset_token: token })
      .catch(() => {
        throw new NotFound("User not found");
      });
    const template = templateForgetPassword(token);
    await sendEmail(body.email, template);
    return { email: body.email };
  }
  async reset(ctx) {
    const { token, password } = ctx.request.body;

    const newPassword = await encryptPassword(password);

    return User.query()
      .findOne({ password_reset_token: token })
      .patch({
        password: newPassword,
        password_reset_token: null,
      })
      .catch(() => {
        throw new NotFound("User not found");
      });
  }
  async create(data) {
    // return User.query().insert({
    //   name: data.name,
    //   password: await encryptPassword(data.password),
    //   email: data.email,
    //   role_id: data.role_id,
    // })
    data.password = await encryptPassword(data.password);
    return userRepo.create(data);
  }
  async update(ctx) {
    const { body: data } = ctx.request;
    return User.query().patchAndFetchById(ctx.params.id, {
      name: body.name,
      email: body.email,
      password: await encryptPassword(body.password),
      role_id: body.role_id,
    });
  }

  async getByIndex() {
    return User.query().withGraphFetched("role");
  }

  async show(id) {
    User.query().findOne({ id }).withGraphFetched("role");
  }

  async destroy(id) {
    return User.query().deleteById(id).returning("*");
  }
}
const userService = new UserService();
export default userService;
