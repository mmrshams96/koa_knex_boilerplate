import Router from "koa-router";

import UserController from "./Controller";
import UserValidate from "./Validator";

const v1 = new Router({ prefix: "/v1" });

v1.get("/me", UserController.me)
  .get("/users", UserController.getByIndex)
  .post("/users/signup", UserValidate.create(), UserController.create)
  .post("/users/login", UserController.login)
  .post("/users/forget", UserController.forget)
  .post("/users/reset", UserController.reset)
  .get("/users/:id", UserController.show)
  .put("/users/:id", UserValidate.update(), UserController.update)
  .delete("/users/:id", UserController.destroy);

export default v1.routes();
