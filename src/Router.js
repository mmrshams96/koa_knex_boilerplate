import Router from "koa-router";

import users from "./app/user/Router";

const router = new Router();
const api = new Router();

api.use(users);

router.use(api.routes());

export default router;
