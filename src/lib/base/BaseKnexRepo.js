import module from "../../helpers/knex";
import { v4 as uuidv4 } from "uuid";
const { knex } = module;
// const knexPaginator = require('@modules/utils/lib/knex-paginator')(knex)
import { AppError, dbUtils } from "../../utils";

class baseRepo {
  constructor({ tableName = null }) {
    if (!tableName) throw Error("table name must be provided");
    this.tableName = tableName;
  }
  generateUuid() {
    return uuidv4();
  }

  async create(entityData, returnFields = "*") {
    try {
      entityData.id = this.generateUuid();
      const id = await knex(this.tableName)
        .insert(entityData)
        .returning(returnFields);
      return id;
    } catch (err) {
      switch (err.code) {
        case "23505":
          throw AppError().DUPLICATED_KEY;
        case "23503":
          throw AppError().FOREIGN_KEY;
      }
      throw err;
    }
  }

  async bulkCreate(entityData) {
    const isInserted = await knex
      .batchInsert(this.tableName, entityData)
      .returning("*");
    return isInserted;
  }

  async findById(id) {
    const [entity] = await knex.select().from(this.tableName).where({ id });
    return entity;
  }

  async findByQuery(query, selectFields = "*") {
    const { pageSize, pageNumber, q, orderBy = [], ...filterQuery } = query;

    let knexQuery = knex
      .select(selectFields)
      .from(this.tableName)
      .where(filterQuery)
      .orderBy(orderBy);

    if (pageNumber)
      if (q) {
        // knexQuery = knexPaginator(knexQuery, pageSize || 10, pageNumber);
        const search = [["organization.name", "ilike", `%${q}%`]];
        knexQuery.where(...search[0]);
      }
    const result = await knexQuery;
    return result;
  }

  async updateById(id, entityData, returnFields = "*") {
    try {
      const entity = await knex(this.tableName)
        .where({ id })
        .update(entityData)
        .returning(returnFields);

      if (!entity.length) throw AppError().RECORD_NOT_FOUND;

      return entity;
    } catch (err) {
      switch (err.code) {
        case "23505":
          throw AppError().DUPLICATED_KEY;
      }
      throw err;
    }
  }

  async updateByQuery(query, entityData, returnFields = "*") {
    try {
      const entity = await knex(this.tableName)
        .where(query)
        .update(entityData)
        .returning(returnFields);

      if (!entity.length) throw AppError().RECORD_NOT_FOUND;

      return entity;
    } catch (err) {
      switch (err.code) {
        case "23505":
          throw AppError().DUPLICATED_KEY;
      }
      throw err;
    }
  }

  async bulkUpdateByIds(ids, entityData, returnFields = "*") {
    const result = await knex(this.tableName)
      .whereIn("id", ids)
      .update(entityData)
      .returning(returnFields);
    return result;
  }

  async bulkUpsert(entities) {
    const columns = dbUtils.getColumnFromObject(entities[0]);
    const values = entities.map(dbUtils.getValuesFromObject);

    console.log("tableName:", this.tableName);
    console.log("columns:", columns);
    console.log("values:", values);

    const result = await knex.raw(
      dbUtils.generateRawUpsertQuery(this.tableName, columns, values)
    );

    return result;
  }

  async deleteById(id) {
    const result = await knex(this.tableName).where({ id }).del();
    return !!result;
  }

  async deleteByQuery() {}

  async bulkDeleteById(ids) {
    const result = await knex(this.tableName).whereIn("id", ids).del();
    return !!result;
  }
}

export default baseRepo;
