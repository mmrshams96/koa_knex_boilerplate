import bcrypt from "bcryptjs";
import crypto from "crypto";
import User from "../../models/User";
import { templateForgetPassword } from "utils";
import userService from "./Service";
import {
  Unauthorized,
  encryptPassword,
  generateJWTToken,
  sendEmail,
  NotFound,
} from "helpers";

export const login = async (ctx) => {
  const { body: data } = ctx.request;
  return userService.login(data);
};

export const getByIndex = () => {
  return userService.getByIndex();
};

export const forget = async (ctx) => {
  const { body: data } = ctx.request;
  return userService.forget(data);
};

export const reset = async (ctx) => {
  const { token, password } = ctx.request.body;
  return userService.reset(data);
};

export const show = (ctx) => {
  const { id } = ctx.params;
  return userService.show(id);
};

export const create = async (ctx) => {
  const { body: data } = ctx.request;
  return userService.create(data);
};

export const update = async (ctx) => {
  const { body } = ctx.request;
  return userService.update();
};

export const destroy = (ctx) => {
  const { id } = ctx.status.user;
  return userService.destroy(id);
};

export const me = (ctx) => {
  const { id } = ctx.status.user;
  return userService.getByIndex();
};

export default {
  getByIndex,
  create,
  login,
  forget,
  reset,
  update,
  show,
  destroy,
  me,
};
