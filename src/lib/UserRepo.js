import BaseKnexRepo from "./base/BaseKnexRepo";

class UserRepo extends BaseKnexRepo {
  constructor() {
    super({ tableName: "users" });
  }
}

export default UserRepo;
