# Node API boilerplate by omid.shams

A boilerplate for building RESTful APIs using Node.js, PostgreSQL, koa, knex, objection.

# Getting started

## Installation

1. Install [Node.JS](https://nodejs.org/en/) LTS version
2. Install PostgreSQL
3. Clone this repository and enter on the respective folder
4. Install dependencies running: `yarn` or `npm install`

## Things to do before run the project:

1. Create database (by follow the commands):

- CREATE USER `user` WITH PASSWORD `password`
- CREATE DATABASE `database`
- GRANT ALL PRIVILEGES ON DATABASE `database` to `user`

2. Install `citext` extension on your database with the command `CREATE EXTENSION citext;`
3. Change name value of .env.example to .env and set the key SECRET to any value you wish
4. Change DATABASE_URL to postgress://`user`:`password`@`localhost`/`database`
5. Run migrations: `yarn db:migrate`
6. Run seeds: `yarn db:seed`
7. Run `yarn start` or `yarn dev` to start server

## Directory Structure

```
├── /src
|   ├── /app
|   |    ├── /user
|   |    |   ├── /validator
|   |    |   ├── /controller
|   |    |   ├── /router
|   |    |   ├── /logic
|   ├── /database
|   |    ├── /migrations
|   |    ├── /seeds
|   ├── /helpers
|   ├── /middleware
|   ├── /models
```
